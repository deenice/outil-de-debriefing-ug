<?php
include_once('functions.php');
$data = read_all_files('DATA');
$imagesExt = array('jpg', 'png', 'bmp', 'tiff');
$videosExt = array('mp4', 'ogg');
$filesPerTeam = array();
foreach($data['files']['all'] as $file){
	$tmp = explode('_', $file);
	if(sizeof($tmp)>1) $team = $tmp[1];
	$ext = pathinfo($file, PATHINFO_EXTENSION);
	if(!strstr($file, 'notepad'))$filesPerTeam[$team][] = $file;
}
ksort($filesPerTeam);

if( (isset($_GET['page']) && $_GET['page'] == 'medias' && !isset($_GET['sortBy'])) || !isset($_GET['page'])){
	header('Location: ?page=medias&sortBy=challenges');
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		
		<title>Urbangaming Debriefing</title>
		
		<link href="cyborg/bootstrap.min.css" rel="stylesheet">
		
		<link rel="stylesheet" type="text/css" href="lightview/css/lightview/lightview.css"/>
		
		<style>
			body {
				padding-top: 85px;
			}
			.well {
				background-color: #222;
			}
			.well img, .well video {
				 border: 0px solid #fff;
			}
			.lightview {
				display: block;
				position: relative;
			}
			.lightview .glyphicon {
				font-size: 48px;
				position: absolute;
				left: 50%;
				top: 50%;
				margin-left: -24px;
				margin-top: -24px;
				color: #fff;
			}
			.lightview .shadow {
				width: 100%;
				height: 100%;
				position: absolute;
				left: 0;
				top: 0;
				bottom: 0;
				right: 0;
				moz-box-shadow: inset 0px 0px 40px 0px #000;
				-webkit-box-shadow: inset 0px 0px 40px 0px #000;
				-o-box-shadow: inset 0px 0px 40px 0px #000;
				box-shadow: inset 0px 0px 40px 0px #000;
				filter:progid:DXImageTransform.Microsoft.Shadow(color=#000, Direction=NaN, Strength=40);
			}
			h1, h2, h3, h4, h5, h6, .lv_title {
				color: #ffbf08;
				text-transform: uppercase;
			}
			.lv_title {
				font-size: 20px;
				margin-bottom: 5px;
			}
			.lv_urbangaming {
				position: relative;
			}
			.lv_urbangaming .logo {
				position: absolute;
				opacity: 0.75;
				width: 200px;
				height: 36px;
				right: 10px;
				bottom: 10px;
				background: url('logo.png') 0px 0px no-repeat transparent;
			}
			.caption {
				position: relative;
			}
			.caption button.hideMedia {
				position: absolute;
				right: 0;
				bottom: 0;
			}
			.caption .label, .lv_caption .label {
				display: none;
			}
			.navbar {
				height: 65px;
			}
			.navbar ul.navbar-nav li a {
				font-size: 24px;
			}
			table#standing tbody tr td .btn-group button {
				display: none;
			}
			table#standing tbody tr:hover td .btn-group button {
				display: block;
			}
			table#standing tbody tr:first-child {
				font-weight: bold;
				color: #ffbf08;
			}
			table#standing tr th, table#standing tr td {
				font-size: 1.6em;
			}
		</style>
	</head>
	
	<body>
		<div class="container-fluid">
			<div class="navbar navbar-inverse navbar-fixed-top">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="?page=medias" style="">
						<?php if(file_exists("CUSTOM/logo.png")): ?>
						<img src="CUSTOM/logo.png" height="36px">
						<?php else: ?>
						<img src="logo.png" height="36px">
						<?php endif; ?>
					</a>
				</div>
				<div class="navbar-collapse collapse navbar-inverse-collapse navbar-right">
					<ul class="nav navbar-nav">
						<li class="<?php print ($_GET['page'] == 'medias') ? 'active' : ''; ?>"><a href="?page=medias"><span class="glyphicon glyphicon-th"></span></a></li>
						<li class="<?php print ($_GET['page'] == 'standings') ? 'active' : ''; ?>"><a href="?page=standings"><span class="glyphicon glyphicon-stats"></span></a></li>
					</ul>
				</div>
			</div>
			
			<?php if(isset($_GET['page'])): ?>
			
			<?php if($_GET['page'] == 'medias'): ?>
			<div class="btn-toolbar" id="sort" style="margin-bottom: 20px;">
				<div class="btn-group">
					<button type="button" class="btn btn-default<?php echo ($_GET['sortBy'] == '' || $_GET['sortBy'] == 'challenges') ? ' active':''; ?>" data-href="?page=medias&sortBy=challenges">Trier par défi</button>
					<button type="button" class="btn btn-default<?php echo ($_GET['sortBy'] == 'teams') ? ' active':''; ?>" data-href="?page=medias&sortBy=teams">Trier par équipe</button>
					<button type="button" class="btn btn-default<?php echo ($_GET['sortBy'] == 'all') ? ' active':''; ?>" data-href="?page=medias&sortBy=all">Voir tout</button>
				</div>
			</div>
			
			<?php if(isset($_GET['sortBy'])): ?>
				
			<?php if($_GET['sortBy'] == 'challenges'): ?>
			<div id="challenges">
			<?php foreach($data['challenges'] as $chKey => $challengeName): $challengeKey = $challengeName . DIRECTORY_SEPARATOR ?>
				<?php if($challengeName != 'notepad' && sizeof($data['files'][$challengeKey]) > 0): ?>
				<div class="well well-sm">
				<h3><?php print $challengeName; ?></h3>
				</div>
				<div class="row">
				<?php foreach($data['files'][$challengeKey] as $fKey => $file): $tmp = explode('_', $file); $team = $tmp[1]; $ext = pathinfo($file, PATHINFO_EXTENSION); ?>
					<div class="col-md-4 col-sm-4 col-lg-3 col-xs-6">
						<div class="well">
							<?php
							$buttons = "<div class='btn-group'>";
							$buttons .= "<button type='button' class='btn btn-default btn-sm' data-team='$team' data-op='minus'>";
							$buttons .= "<span class='glyphicon glyphicon-minus'></span></button>";
							$buttons .= "<button type='button' class='btn btn-default btn-sm' data-team='$team' data-op='plus'>";
							$buttons .= "<span class='glyphicon glyphicon-plus'></span></button></div>";
							$buttons .= "<span class='label label-success'>+1</span><span class='label label-danger'>-1</span>";
							$buttons .= "<script type='text/javascript'>";
							$buttons .= "$('.lv_caption button').click(function(e){
										var team = $(this).data('team');
										var op = $(this).data('op');
										
										if(window.localStorage.getItem(team)){
											var actualPoints = parseInt(window.localStorage[team]);
										} else {
											var actualPoints = 0;
										}
										
										if(op == 'plus'){
											var newPoints = actualPoints + 1;
											$(this).parents('.lv_caption').find('.label-success').fadeIn(50).delay(500).fadeOut(200);
										}
										if(op == 'minus'){
											var newPoints = actualPoints - 1;
											$(this).parents('.lv_caption').find('.label-danger').fadeIn(50).delay(500).fadeOut(200);
										}
										if(op == 'reset'){
											var newPoints = 0;
										}
										window.localStorage.setItem(team, newPoints);})";
							$buttons .= "</script>";
							?>
							<?php if(in_array($ext, $imagesExt)): ?>
								<a href="<?php print $file; ?>" class="lightview" data-lightview-caption="<?php print $buttons; ?>" data-lightview-group="<?php print $challengeName; ?>" data-lightview-title="<?php print $team; ?>" data-lightview-group-options="continuous: true, wrapperClass: 'lv_urbangaming', afterUpdate: function(e,p){ $('.lv_urbangaming').append('<div class=logo></div>'); }">
									<span class="glyphicon glyphicon-camera"></span>
									<img src="<?php print $file; ?>" class="img-responsive">
									<div class="shadow"></div>
								</a>
							<?php elseif(in_array($ext, $videosExt)): $rand=rand(1,1000);?>
								<a class="lightview" title="<?php print $team; ?>" href="<?php print $file; ?>" data-lightview-caption="<?php print $buttons; ?>" data-lightview-title="<?php print $team; ?>" data-lightview-group="<?php print $challengeName; ?>" data-lightview-group-options="continuous: true, width:1600,height: 900">
									<span class="glyphicon glyphicon-facetime-video"></span>
									<video src="<?php print $file; ?>" class="img-responsive"></video>
									<div class="shadow"></div>
									<div id="video<?php print $rand; ?>" style="display:none">
										<video controls>
											<source src="<?php print $file; ?>" type="video/mp4">
										</video>
									</div>
								</a>
							<?php endif; ?>
							<div class="caption">
								<h4><?php print $team ;?></h4>
								<div class="btn-group">
									<button type="button" class="btn btn-default btn-sm" data-team="<?php print $team; ?>" data-op="minus"><span class="glyphicon glyphicon-minus"></span></button>
									<button type="button" class="btn btn-default btn-sm" data-team="<?php print $team; ?>" data-op="plus"><span class="glyphicon glyphicon-plus"></span></button>
								</div>
								<button type="button" class="btn btn-default btn-sm hideMedia" data-file="<?php print $file; ?>"><span class="glyphicon glyphicon-eye-close"></span></button>
								<span class="label label-success">+1</span>
								<span class="label label-danger">-1</span>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
				<?php endif; ?>
			<?php endforeach; ?>
			</div>
			
			<?php elseif($_GET['sortBy'] == 'teams'): ?>
			<div id="teams">
				<?php foreach($filesPerTeam as $teamName => $files): asort($files)?>
					<div class="well well-sm">
					<h3><?php print $teamName; ?></h3>
					</div>
					<div class="row">
					<?php foreach($files as $fKey => $file): $tmp = explode('_', $file); $tmp1 = explode(DIRECTORY_SEPARATOR,$tmp[0]); $challenge = end($tmp1); $ext = pathinfo($file, PATHINFO_EXTENSION); ?>
						<div class="col-md-4 col-sm-4 col-lg-3 col-xs-6">
							<div class="well">
								<?php
								$buttons = "<div class='btn-group'>";
								$buttons .= "<button type='button' class='btn btn-default btn-sm' data-team='$teamName' data-op='minus'>";
								$buttons .= "<span class='glyphicon glyphicon-minus'></span></button>";
								$buttons .= "<button type='button' class='btn btn-default btn-sm' data-team='$teamName' data-op='plus'>";
								$buttons .= "<span class='glyphicon glyphicon-plus'></span></button></div>";
								$buttons .= "<span class='label label-success'>+1</span><span class='label label-danger'>-1</span>";
								$buttons .= "<script type='text/javascript'>";
								$buttons .= "$('.lv_caption button').click(function(e){
											var team = $(this).data('team');
											var op = $(this).data('op');
											
											if(window.localStorage.getItem(team)){
												var actualPoints = parseInt(window.localStorage[team]);
											} else {
												var actualPoints = 0;
											}
											
											if(op == 'plus'){
												var newPoints = actualPoints + 1;
												$(this).parents('.lv_caption').find('.label-success').fadeIn(50).delay(500).fadeOut(200);
											}
											if(op == 'minus'){
												var newPoints = actualPoints - 1;
												$(this).parents('.lv_caption').find('.label-danger').fadeIn(50).delay(500).fadeOut(200);
											}
											if(op == 'reset'){
												var newPoints = 0;
											}
											window.localStorage.setItem(team, newPoints);})";
								$buttons .= "</script>";
								?>
								<?php if(in_array($ext, $imagesExt)): ?>
									<a href="<?php print $file; ?>" class="lightview" data-lightview-caption="<?php print $buttons; ?>" data-lightview-group="<?php print $teamName; ?>" data-lightview-title="<?php print $challenge; ?>" data-lightview-group-options="continuous: true, wrapperClass: 'lv_urbangaming', afterUpdate: function(e,p){ $('.lv_urbangaming').append('<div class=logo></div>') }">
										<span class="glyphicon glyphicon-camera"></span>
										<img src="<?php print $file; ?>" class="img-responsive">
										<div class="shadow"></div>
									</a>
								<?php elseif(in_array($ext, $videosExt)): $rand=rand(1,1000);?>
									<a class="lightview" title="<?php print $teamName; ?>" href="<?php print $file; ?>" data-lightview-caption="<?php print $buttons; ?>" data-lightview-title="<?php print $challenge; ?>" data-lightview-group="<?php print $teamName; ?>" data-lightview-group-options="continuous: true, width:1600,height: 900">
										<span class="glyphicon glyphicon-facetime-video"></span>
										<video src="<?php print $file; ?>" class="img-responsive"></video>
										<div class="shadow"></div>
										<div id="video<?php print $rand; ?>" style="display:none">
											<video controls>
												<source src="<?php print $file; ?>" type="video/mp4">
											</video>
										</div>
									</a>
								<?php endif; ?>
								<div class="caption">
									<h4><?php print $challenge ;?></h4>
									<div class="btn-group">
										<button type="button" class="btn btn-default btn-sm" data-team="<?php print $teamName; ?>" data-op="minus"><span class="glyphicon glyphicon-minus"></span></button>
										<button type="button" class="btn btn-default btn-sm" data-team="<?php print $teamName; ?>" data-op="plus"><span class="glyphicon glyphicon-plus"></span></button>
									</div>
									<span class="label label-success">+1</span>
									<span class="label label-danger">-1</span>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
					</div>
				<?php endforeach; ?>
			</div>
			
			<?php elseif($_GET['sortBy'] == 'all'): ?>
			<div id="all">
				<div class="row">
				<?php foreach($data['files']['all'] as $fKey => $file): $tmp = explode('_', $file); $tmp1 = explode(DIRECTORY_SEPARATOR,$tmp[0]); $challenge = end($tmp1); $ext = pathinfo($file, PATHINFO_EXTENSION);?>
					<?php if(!strstr($file, 'notepad')): ?>	
						<div class="col-md-4 col-sm-4 col-lg-3 col-xs-6">
							<div class="well">
								<?php if(in_array($ext, $imagesExt)): ?>
									<a href="<?php print $file; ?>" class="lightview" data-lightview-group="<?php print $challengeName; ?>" data-lightview-title="<?php print $team; ?>" data-lightview-group-options="wrapperClass: 'lv_urbangaming', afterUpdate: function(e,p){ $('.lv_urbangaming').append('<div class=logo></div>') }">
										<span class="glyphicon glyphicon-camera"></span>
										<img src="<?php print $file; ?>" class="img-responsive">
										<div class="shadow"></div>
									</a>
								<?php elseif(in_array($ext, $videosExt)): ?>
									<a class="lightview" title="<?php print $team; ?>" href="<?php print $file; ?>" data-lightview-caption="<?php print $buttons; ?>" data-lightview-title="<?php print $team; ?>" data-lightview-group="<?php print $challengeName; ?>" data-lightview-group-options="continuous: true, width:1600,height: 900">
										<span class="glyphicon glyphicon-facetime-video"></span>
										<video src="<?php print $file; ?>" class="img-responsive"></video>
										<div class="shadow"></div>
									</a>
								<?php endif; ?>
								<div class="caption">
									<h4><?php print $tmp[1]; ?></h4>
									<h4><?php print $challenge ;?></h4>
								</div>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			
			<?php endif; ?>
			
			<?php elseif($_GET['page'] == 'standings'): ?>
				
			<h2>Classement</h2>
				
			<table class="table table-striped table-hover" id="standing">
				<thead>
					<tr>
						<th style="width: 40px">#</th>
						<th style="width: 300px">Nom d'équipe</th>
						<th>Points</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1; foreach($filesPerTeam as $teamName => $file): ?>
					<tr data-point="" data-team="<?php print $teamName ?>">
						<td class="position"><?php print $i; ?></td>
						<td><?php print $teamName; ?></td>
						<td class="point"></td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs" data-team="<?php print $teamName; ?>" data-op="reset"><span class="glyphicon glyphicon-repeat"></span></button>
								<button type="button" class="btn btn-default btn-xs" data-team="<?php print $teamName; ?>" data-op="minus"><span class="glyphicon glyphicon-minus"></span></button>
								<button type="button" class="btn btn-default btn-xs" data-team="<?php print $teamName; ?>" data-op="plus"><span class="glyphicon glyphicon-plus"></span></button>
							</div>
						</td>
					</tr>
					<?php $i++; endforeach; ?>
				</tbody>
			</table> 
			<?php endif; ?>
			
			<?php endif; ?>
			
		</div>
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="js/jquery.min.js"></script>
	    <script type="text/javascript" src="js/swfobject.js"></script>
	    <script src="js/bootstrap.min.js"></script>
	    
	    <script type="text/javascript" src="lightview/js/spinners/spinners.min.js"></script>
		<script type="text/javascript" src="lightview/js/lightview/lightview.js"></script>
		
		<script type="text/javascript" src="js/jquery.sortElements.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){

				function sortTable(table, callback){
					var rows = table.find('tbody tr').get();
					rows.sort(function(a, b) {
						var A = $(a).data('point');
						var B = $(b).data('point');
						if(A < B) {
							return 1;
						}
						if(A > B) {
							return -1;
						}
						return 0;
					});
					$.each(rows, function(index, row) {
						table.children('tbody').append(row);
					});
					//table.find('tbody tr:first').addClass('text-success').find('td').css('font-weight: bold');
					callback();
				}
				
				$('#sort button').click(function(event){
					var href = $(this).data('href');
					document.location.href = href;
				})
				
				$('#standing button, .caption .btn-group button, .lv_caption button').click(function(event){
					
					event.preventDefault();

					var team = $(this).data('team');
					var op = $(this).data('op');
					
					if(window.localStorage.getItem(team)){
						var actualPoints = parseInt(window.localStorage[team]);
					} else {
						var actualPoints = 0;
					}
					
					if(op == 'plus'){
						var newPoints = actualPoints + 1;
						$(this).parents('.caption').find('.label-success').fadeIn(50).delay(500).fadeOut(200);
					}
					if(op == 'minus'){
						var newPoints = actualPoints - 1;
						$(this).parents('.caption').find('.label-danger').fadeIn(50).delay(500).fadeOut(200);
					}
					if(op == 'reset'){
						var newPoints = 0;
					}
					window.localStorage.setItem(team, newPoints);
					
					$('tr[data-team="'+team+'"] td.point').text(newPoints);
					$('tr[data-team="'+team+'"]').attr('data-point', newPoints);
					
					/*sortTable($('table#standing'), function(){
						$('table#standing tbody tr').each(function(i,e){
							$(e).find('td.position').text(i+1);
						})
					});*/
					
				})
				
				if($('table#standing').length){
					$('table#standing tbody tr').each(function(i,e){
						$(this).find('td.point').text(window.localStorage[$(this).data('team')]);
						$(this).attr('data-point', window.localStorage[$(this).data('team')]);
					})
					
					sortTable($('table#standing'), function(){
						$('table#standing tbody tr').each(function(i,e){
							$(e).find('td.position').text(i+1);
						})
					});
				}
				
				$('button.hideMedia').click(function(event){
					event.preventDefault();
					var container = $(this).parents('.well');
					var uri = $(this).attr('data-file');
					var tmp = uri.split('<?php print DIRECTORY_SEPARATOR; ?>');
					var filename = tmp[tmp.length-1];
					$.ajax({
						url: 'ajax.php',
						type: 'post',
						data: {
							uri: uri,
							function: 'renameMedia'
						},
						success: function(data){
							container.stop().animate({
								'opacity': 0.3
							}, 'fast')
						}
					})
				})
				
			})
			
		</script>
	</body>
</html>
